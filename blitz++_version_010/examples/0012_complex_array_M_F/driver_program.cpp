
//========================//
// Complex arrays example //
//========================//

//===============//
// code --> 0012 //
//===============//

// keywords: zip, tensor, array

#include <blitz/array.h>
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

BZ_USING_NAMESPACE(blitz)

const double pi = 3.14159265358979323846264338327950288;

// the mian function without complex support

#ifndef BZ_HAVE_COMPLEX
int main()
{
	cout << "Complex number support is required to compile this example" << endl;
     
    	return 0;
}

#else

// the main function with complex support

int main()
{

	const int K_MAX = 10000;

	for (int k = 0; k != K_MAX; k++)
	{
		cout << "---------------------------------------------------->> " << k << endl;

		const int N = static_cast<long>(pow(10.0, 8.0));

		Array<complex<double>,1> A(N);
    		Array<double,1> theta(N);
		firstIndex i;

		cout << fixed;
		cout << setprecision(20);
		cout << showpos;
		cout << showpoint;

    		//BZ_USING_NAMESPACE(blitz::tensor); // it is not needed

    		// Fill the theta array with angles from 0..2 Pi, evenly spaced
 
		cout << " --> 1 --> build --> Array<double,1> theta(N)" << endl;
 
		theta = (2 * pi * i) / N;

    		// Set A[i] = cos(theta[i]) + _I * sin(theta[i])
 
		cout << " --> 2 --> build --> Array<complex<double>,1> A(N)" << endl;
 
		A = zip(cos(theta), sin(theta), complex<double>());

    		cout << A(1,1) << endl;
		cout << A(11,12) << endl;

		#ifdef BZ_HAVE_COMPLEX_MATH1
    
		// Here's another way of doing it, which eliminates the need for
    		// the theta array:
    		// Set A[i] = exp(Pi i _I / N)
    
		cout << " --> 3 --> build --> Array<complex<double>,1> A(N)" << endl;
	
		A = exp(zip(0.0, (2 * pi * i) / N, complex<double>()));

    		cout << A(1,1) << endl;
		cout << A(11,12) << endl;
		
		#endif
		
		cout << " --> 4 --> restart" << endl;
	}

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

#endif // BZ_HAVE_COMPLEX


//======//
// FINI //
//======//

