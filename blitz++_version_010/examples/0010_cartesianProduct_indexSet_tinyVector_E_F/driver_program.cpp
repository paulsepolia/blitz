
//===============//
// cartesian.cpp //
//===============//

//===============//
// code --> 0010 //
//===============//

// keywords: list, CartecianProduct, TinyVector, indexSet,
//	     CartecianProduct::iterator

#include <blitz/array.h>
#include <blitz/array/cartesian.h>
#include <iostream>
#include <list>

using std::endl;
using std::cin;
using std::cout;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	// list

	list<int> index1;

    	index1.push_back(5);
    	index1.push_back(7);
    	index1.push_back(9);

	// list

    	list<int> index2;

    	index2.push_back(1);
    	index2.push_back(2);
    	index2.push_back(4);
    	index2.push_back(7);

	// CartesianProduct, indexSet

    	CartesianProduct<TinyVector<int,2>,list<int>,2> indexSet(index1,index2);
 
	// CartesianProduct::iterator

   	typedef CartesianProduct<TinyVector<int,2>,list<int>,2>::iterator T_iterator;

    	T_iterator iter = indexSet.begin();
	T_iterator end = indexSet.end();

    	for (; iter != end; ++iter)
        {
		cout << *iter << endl;
	}

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

    	return 0;

	cout << " --> exit" << endl;
}

//======//
// FINI //
//======//

