
//===============//
// cartesian.cpp //
//===============//

//===============//
// code --> 0011 //
//===============//

// keywords: list, CartecianProduct, TinyVector, indexSet,
//	     CartecianProduct::iterator

#include <blitz/array.h>
#include <blitz/array/cartesian.h>
#include <iostream>
#include <list>
#include <cmath>

using std::endl;
using std::cin;
using std::cout;
using std::pow;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	// local variables and parameters

	const int K_MAX = 100000;
	const int I_MAX1 = 5 * static_cast<int>(pow(10.0, 7.0));
	const int I_MAX2 = 5 * static_cast<int>(pow(10.0, 7.0));

	// main for loop

	for (int k = 0; k != K_MAX; k++)
	{

		cout << "------------------------------------------------->> " << k << endl;

		// list --> index1

		list<int> * index1 = new list<int>;

		cout << " --> 1 --> build list index1" << endl;

		for (int i = 0; i != I_MAX1; i++)
		{
    			index1->push_back(i);
		}

		// list --> index2

    		list<int> * index2 = new list<int>;

		cout << " --> 2 --> build list index2" << endl;

		for (int i = 0; i != I_MAX2; i++)
                {
                        index2->push_back(i+1);
                }

		// CartesianProduct, indexSet

		cout << " --> 3 --> the cartesian product" << endl;

    		CartesianProduct<TinyVector<int,2>,list<int>,2> indexSet(*index1, *index2);
 
		// CartesianProduct::iterator

   		typedef CartesianProduct<TinyVector<int,2>,list<int>,2>::iterator T_iterator;

    		T_iterator iter = indexSet.begin();
		T_iterator end = indexSet.end();

		cout << " --> 4 --> some output" << endl;

		int counter = 0;
	
    		for (; iter != indexSet.end(); ++iter)
        	{
			counter++;

			if (counter == 10) break;

			cout << *iter << endl;
		}

		// delete the list containers

		cout << " --> 5 --> delete list index1" << endl;

		delete index1;

		cout << " --> 6 --> delete list index2" << endl;	

		delete index2;
	}

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

    	return 0;

	cout << " --> exit" << endl;
}

//======//
// FINI //
//======//

