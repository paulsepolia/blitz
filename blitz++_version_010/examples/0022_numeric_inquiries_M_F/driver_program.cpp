
//======================================================//
// numinquire.cpp     					//
// Blitz++ <numinquire.h> example, illustrating how to	//
// get at properties of numeric types			//
//======================================================//

//===============//
// code --> 0022 //
//===============//

// keywords: digits, epsilon, huge, tiny, max_exponent, min_exponent

#include <blitz/blitz.h>
#include <blitz/numinquire.h>
#include <iomanip>

using std::boolalpha;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{

	cout << boolalpha;

	long double z = 1.0;

	cout << endl;
    	cout << " --> some inquiries into the nature of double" << endl;
	cout << endl;

        cout << " --> digits(z)         = " << digits(z)         << endl;
        cout << " --> epsilon(z)        = " << epsilon(z)        << endl;
        cout << " --> huge(z)           = " << huge(z)           << endl;
        cout << " --> tiny(z)           = " << tiny(z)           << endl;
        cout << " --> max_exponent(z)   = " << max_exponent(z)   << endl;
        cout << " --> min_exponent(z)   = " << min_exponent(z)   << endl;
        cout << " --> max_exponent10(z) = " << max_exponent10(z) << endl;
        cout << " --> min_exponent10(z) = " << min_exponent10(z) << endl;
        cout << " --> precision(z)      = " << precision(z)      << endl;
        cout << " --> radix(z)          = " << radix(z)          << endl;

    	Range r = range(z);

    	cout << " --> range(z) = [ " 
             << r.first() 
	     << ", " 
             << r.last() 
             << " ]" << endl;
    
	cout << endl;
	cout << " --> more obscure properties" << endl;
	cout << endl;     

        cout << " --> is_integer(z)        = " << is_integer(z)        << endl;
        cout << " --> is_exact(z)          = " << is_exact(z)          << endl;
        cout << " --> round_error(z)       = " << round_error(z)       << endl;
        cout << " --> has_infinity(z)      = " << has_infinity(z)      << endl;
        cout << " --> has_quiet_NaN(z)     = " << has_quiet_NaN(z)     << endl;
        cout << " --> has_signaling_NaN(z) = " << has_signaling_NaN(z) << endl;
        cout << " --> has_denorm(z)        = " << has_denorm(z)        << endl;
        cout << " --> has_denorm_loss(z)   = " << has_denorm_loss(z)   << endl;
        cout << " --> infinity(z)          = " << infinity(z)          << endl;
        cout << " --> quiet_NaN(z)         = " << quiet_NaN(z)         << endl;
        cout << " --> signaling_NaN(z)     = " << signaling_NaN(z)     << endl;
        cout << " --> denorm_min(z)        = " << denorm_min(z)        << endl;
        cout << " --> is_iec559(z)         = " << is_iec559(z)         << endl;
        cout << " --> is_bounded(z)        = " << is_bounded(z)        << endl;
        cout << " --> is_modulo(z)         = " << is_modulo(z)         << endl;
        cout << " --> traps(z)             = " << traps(z)             << endl;
        cout << " --> tinyness_before(z)   = " << tinyness_before(z)   << endl;

    	return 0;
}

