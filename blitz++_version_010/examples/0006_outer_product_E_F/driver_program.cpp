
//=================================================//
// outer.cpp        				   //
// Blitz++ outer product (tensor notation) example //
//=================================================//

//===============//
// code --> 0006 //
//===============//

// keywords: outer product, tensor notation, Array

#include <iostream>
#include <ctime>
#include <iomanip>
#include <blitz/array.h>

using std::endl;
using std::cout;
using std::cin;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	// local parameters and variables

	const int K_MAX = 100000;
	const int N = 20000;
	firstIndex i;
	secondIndex j;
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << showpos;
	cout << showpoint;
	cout << setprecision(6);

	// main for loop

	for (int k = 0; k != K_MAX; k++)
	{
		cout << "--------------------------------------------------->> " << k << endl;

		cout << " -->  1 --> declare arrays" << endl;

		Array<double, 1> x(N);
		Array<double, 1> y(N);
    		Array<double, 2> A(N,N);

		cout << " -->  2 --> build --> x" << endl;

		x = 1.0;

		cout << " -->  3 --> build --> y" << endl;

		y = 2.0;

		cout << " -->  4 --> A = x*y --> outer product" << endl;

		t1 = clock();

    		A = x(i) * y(j);

		t2 = clock();

		cout << " --> time used = " 
		     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " -->  5 --> A = 10.0" << endl;

		t1 = clock();

    		A = 10.0;

		t2 = clock();

                cout << " --> time used = " 
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " -->  6 --> A = A + A + A + A" << endl;

		t1 = clock();

		A = A + A + A + A;

		t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	       	cout << " -->  7 --> A = sin(A) + A + A + A" << endl;

                t1 = clock();

                A = sin(A) + A + A + A;

                t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " -->  8 --> A = sin(A) + sin(A) + A + A"  << endl;

                t1 = clock();

                A = sin(A) + sin(A) + A + A;

                t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

               cout << " -->  9 --> A = sin(A) + sin(A) + sin(A) + sin(A)"  << endl;

                t1 = clock();

                A = sin(A) + sin(A) + sin(A) + sin(A);

                t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " --> 10 --> A = (x*y)+(x*y)+(x*y) --> outer product" << endl;

                t1 = clock();

                A = (x(i)*y(j)) + (x(i)*y(j)) + (x(i) * y(j));

                t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " --> 10 --> last" << endl;
	}

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

