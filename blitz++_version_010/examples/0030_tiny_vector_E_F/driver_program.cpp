//=============================================================================//
// tiny.cpp    Blitz++ TinyVector<T,N> ray reflection example
//
// This example illustrates the TinyVector<T,N> class.  TinyVectors can be
// used for small vectors whose sizes are known at compile time.  Most
// operations on TinyVectors have their loops unravelled inline using template 
// metaprograms.
//
// The routine reflect(..) calculates the reflection of a monochrome ray 
// of light bouncing off a perfectly reflective, smooth surface.  
//=============================================================================//

//===============//
// code --> 0030 //
//===============//

// keywords: TinyVector

#include <blitz/array.h>
#include <blitz/tinyvec2.h>

BZ_USING_NAMESPACE(blitz)

void reflect( TinyVector<double,3>& reflection, 
	      const TinyVector<double,3>& ray,
              const TinyVector<double,3>& surfaceNormal)
{
	// The surface normal must be unit length to use this equation.

    	reflection = ray - 2 * dot(ray,surfaceNormal) * surfaceNormal;
}

// the main function

int main()
{
	const int K_MAX = 5000;

    	TinyVector<double,3> x, y, z;

    	// y will be the incident ray

	for (int k = 0; k != K_MAX; k++)
	{
		y[k] = k;
		z[k] = k+1;
	}

    	reflect(x, y, z);

    	cout << "Reflected ray is: [ " 
             << x[0] 
             << " " 
             << x[1] 
             << " " 
             << x[2]
             << " ]" << endl;

	cout << " --> end" << endl;

	// sentineling;

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

	return 0;
}

//======//
// FINI //
//======//
