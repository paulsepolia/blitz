
//===============================================//
//  matmult.cpp     			         //
//  Blitz++ tensor notation example	         //
//  This example illustrates a building matrices //
//  using Blitz++                                //
//===============================================//

//===============//
// code --> 0004 //
//===============//

// keywords: Range, Array, firstIndex, secondIndex

#include <iostream>
#include <blitz/array.h>

using std::cout;
using std::cin;
using std::endl;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	// local variables and parameters

	const int KK_MAX = 100000;
	const int N1 = 17000;

	// main loop
	
	for (int kk = 1; kk != KK_MAX; kk++)
	{
		cout << " ------------------------------------------------------>> " << kk << endl;

		cout << " -->  1 --> declare indices" << endl;

		firstIndex i;
		secondIndex j;

		cout << " -->  2 --> declare matrix --> A" << endl;

    		Range r(1, N1);
    		Array<double, 2> A(r,r);

		cout << " -->  3 --> build matrix --> A" << endl;

    		A = 1.0 / (i+j-1); // A --> a_ij = 1/(i+j-1)

		cout << " -->  4 --> declare matrix --> B" << endl;

		Array<double, 2> B(r,r);

		cout << " -->  5 --> build matrix --> B" << endl;

    		B = (i == (5-j)); // B --> b_ij = (i == (5-j))

		cout << " -->  6 --> declare matrix --> C" << endl;

    		Array<double, 2> C(r,r);

		cout << " -->  7 --> build matrix --> C = A*B --> 1" << endl;

    		C = A * B; 

		cout << " -->  8 --> build matrix --> C = A+B --> 2" << endl;

		C = A + B; 

		cout << " -->  9 --> build matrix --> C = 10.0 --> 3" << endl;

		C = 10.0;

		cout << " --> 10 --> build matrix --> C = A+B+C --> 4" << endl;

		C = A+B+C;

		cout << " --> 11 --> build matrix --> C = sin(A)+cos(B)+sin(C) --> 5" << endl;

		C = sin(A) + cos(B) + sin(C);

		cout << " --> last" << endl;
	}

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

