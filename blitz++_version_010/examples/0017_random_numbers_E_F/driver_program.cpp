
//================//
// Random numbers //
//================//

//===============//
// code --> 0017 //
//===============//

// keywords: DiscreteUniform, random(), seed, MersenneTwister 

#include <blitz/blitz.h>
#include <random/exponential.h>
#include <random/discrete-uniform.h>
#include <random/F.h>

using namespace blitz;
using namespace ranlib;

#include <ctime>

// function --> main2

int main()
{
	DiscreteUniform<int> rng(100);
    	rng.seed(static_cast<unsigned int>(time(0)));

    	for (int i = 0; i != 100; i++)
	{
		cout << rng.random() << endl;
	}
    
	cout << endl;
    
	return 0;
}

// function --> main3

int main3()
{
 	MersenneTwister x;

	for (int j = 0; j != 1000; j++)
	{
        	cout << x.random() << endl;
    	}

    	return 0;
}

// function --> main()

int main2()
{
	F<long double> rng(2.0,3.0);

    	rng.seed(static_cast<unsigned int>(time(0)),static_cast<unsigned int>(time(0)));

    	long double sum1 = 0;
	long double sum2 = 0;
	long double sum3 = 0;
	long double sum4 = 0;

    	const int N = 10000;

    	for (int i = 0; i != N; i++)
    	{
        	long double x = rng.random();
        	sum1 += x;
        	sum2 += x*x;
        	sum3 += x*x*x;
        	sum4 += x*x*x*x;
    	}

    	cout << "k1 = " << sum1/N << endl
             << "k2 = " << sum2/N << endl
             << "k3 = " << sum3/N << endl
             << "k4 = " << sum4/N << endl;

    	return 0;
}

//======//
// FINI //
//======//

