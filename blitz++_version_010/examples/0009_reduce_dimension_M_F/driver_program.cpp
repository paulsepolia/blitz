
//=======================================================================//
// reduce.cpp     							 //
// Blitz++ array reduction example					 //
//									 //
// This example illustrates the array reduction functions provided by    //
// Blitz++.  These functions reduce an N dimensional array (or array     //
// expression) to an N-1 dimensional array expression by summing, taking //
// the mean, etc.  These array reductions are currently provided: sum,   //
// mean, min, max, minIndex, maxIndex, product, count, any and all.	 //
//									 //
//=======================================================================//

//===============//
// code --> 0009 //
//===============//

// keywords: Array, secondIndex, sum, min, max, mean, minIndex, maxIndex
//	     first, product, count, any

#include <iostream>
#include <blitz/array.h>

using std::cout;
using std::cin;
using std::endl;

BZ_USING_NAMESPACE(blitz)

int main()
{
	Array<int, 2> A(4,4);

    	A = 3,  1,  2,  4,       
            8, -1, -5,  3,       
            0,  9, -1,  4,
            1,  3,  1,  2;

    	cout << " --> A = " << A << endl;

	cout << endl;
	cout << endl;

	cout << " --> A(0,0) = " << A(0,0) << endl;
	cout << " --> A(1,1) = " << A(1,1) << endl;
	cout << " --> A(2,2) = " << A(2,2) << endl;
	cout << " --> A(3,3) = " << A(3,3) << endl;

	cout << endl;
	cout << endl;

    	//
    	// Reduce the array A to a one-dimensional array, 
    	// by summing/taking the mean/etc. of each row
    	//

    	Array<int, 1> z(4);
    	Array<float, 1> z2(4);
    	secondIndex j;

    	z = sum(A, j);
    	
	cout << " --> sum(A,j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z2 = mean(A, j);
    
	cout << " --> mean(A,j) = " << endl;
	cout << endl;
	cout << z2 << endl;

    	z = min(A, j);
    
	cout << " --> min(A,j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = minIndex(A, j);
    
	cout << " --> minIndex(A, j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = max(A, j);
    
	cout << " --> max(A, j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = maxIndex(A, j);
    
	cout << " --> maxIndex(A, j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = first((A < 0), j);
    
	cout << " --> first((A < 0), j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = product(A, j);
    
	cout << " --> product(A, j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = count((A > 0), j);
    
	cout << " --> count((A > 0), j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = any((abs(A) > 4), j);
    
	cout << " --> any((abs(A) > 4), j) = " << endl;
	cout << endl;
	cout << z << endl;

    	z = all(A > 0, j);
    
	cout << " --> all(A > 0, j) = " << endl;
	cout << endl;
	cout << z << endl;

	cout << endl;
	cout << endl;

	cout << " --> z(0) = " << z(0) << endl;
	cout << " --> z(1) = " << z(1) << endl;
	cout << " --> z(2) = " << z(2) << endl;
	cout << " --> z(3) = " << z(3) << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

    	return 0;
}

//======//
// FINI //
//======//

