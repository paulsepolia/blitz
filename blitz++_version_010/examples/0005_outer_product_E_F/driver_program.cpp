
//==================================================//
// outer.cpp        				    //
// Blitz++ outer product (tensor notation) example  //
//==================================================//

//===============//
// code --> 0005 //
//===============//

// keywords: outer product, tensor, notation, Array, firstIndex, secondIndex

#include <iostream>
#include <blitz/array.h>

using std::endl;
using std::cout;
using std::cin;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	Array<double, 1> x(4);
	Array<double, 1> y(4);
    	Array<double, 2> A(4,4);

    	x = 1, 2, 3, 4;
    	y = 1, 0, 0, 1;

    	firstIndex i;
    	secondIndex j;

    	A = x(i) * y(j);

    	cout << A << endl;


	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

