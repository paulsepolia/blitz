
//===========================================//
// define functions and apply them on arrays //
//===========================================//

//===============//
// code --> 0026 //
//===============//

// keywords: declare new functions

#define BZ_NEW_EXPRESSION_TEMPLATES

#include <blitz/array.h>
#include <iostream>
#include <iomanip>

using std::cout;
using std::cin;
using std::endl;
using std::fixed;
using std::setprecision;
using std::showpos;
using std::showpoint;

BZ_USING_NAMESPACE(blitz)

// function --> fA

double fA(double x)
{ 
	return 1.0 / x;
}

// function --> fB

double fB(double x, double y)
{
    return x*y;
}

// declare the above function

BZ_DECLARE_FUNCTION(fA)
BZ_DECLARE_FUNCTION2(fB)

// the main function

int main()
{
	cout << fixed;
	cout << showpos;
	cout << showpoint;
	cout << setprecision(20);

	firstIndex i;
	secondIndex j;

	const int N = 10000;
	const int K_MAX = 10000;

	for (int k = 0; k != K_MAX; k++)
	{
		cout << "-------------------------------------------------->> " << k << endl; 

		Array<double,2> A(N,N);
		Array<double,2> B(N,N);
		Array<double,2> C(N,N);

    		A = 10.0;
       		B = 20.0; 
    		C = 30.0;

		cout << " --> A(1,1) = " << A(1,1) << endl;
                cout << " --> B(1,1) = " << B(1,1) << endl;
                cout << " --> C(1,1) = " << C(1,1) << endl;

    		A = fA(A);
    		B = fB(A, A);
		C = fA(A);

		cout << " --> A(1,1) = " << A(1,1) << endl;
		cout << " --> B(1,1) = " << B(1,1) << endl;
		cout << " --> C(1,1) = " << C(1,1) << endl;
	
		B = fB(fA(A), fA(A));

		cout << " --> B(1,1) = " << B(1,1) << endl;
	}


	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

	return 0;
}

//======//
// FINI //
//======//

