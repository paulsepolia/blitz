
//=========================//
// base and derive classes //
//=========================//

//===============//
// code --> 0016 //
//===============//

// keywords: multiply, array, base, derived, operator overload

#include <blitz/array.h>

using namespace blitz;

// class --> Base
// pure virtual class

class Base 
{
public:
         
	virtual void multiply(Array<int,1> &) const = 0;
};

// class --> Derived1

class Derived1 : public Base 
{
public:

	// constructor

	Derived1() {}
         
	// member function

	void multiply(Array<int,1> & array) const 
	{
		array *= 1;
	}
};

// class --> Derived2

class Derived2 : public Base 
{
public:

	// constructor
         
	Derived2(){}
         
	// member function

	void multiply(Array<int,1> & array) const 
	{
		array *= 2;
	}
       
};

// operator *= definition
       
Array<int,1> & operator *= (Array<int,1> & array, const Derived1& object) 
{
         object.multiply(array);

         return array;
}

// operator *= definition

Array<int,1> & operator *= (Array<int,1> & array, const Derived2 & object)
{
         object.multiply(array);

         return array;
}

// the main function

int main()
{
	//

	Array<int,1> array(5);

	cout << " -->  1 --> array(0) = " << array(0) << endl;
	cout << " -->  2 --> array(1) = " << array(1) << endl;
	cout << " -->  3 --> array(2) = " << array(2) << endl;

	//

       	array = 11;

	cout << " -->  4 --> array(0) = " << array(0) << endl;
        cout << " -->  5 --> array(1) = " << array(1) << endl;
        cout << " -->  6 --> array(2) = " << array(2) << endl;

	// use of operator overload *=

       	Derived1 derived1;

	array *= derived1;

	cout << " -->  7 --> array(0) = " << array(0) << endl;
        cout << " -->  8 --> array(1) = " << array(1) << endl;
        cout << " -->  9 --> array(2) = " << array(2) << endl;

	// use of operator overload *=

       	Derived2 derived2;

        array *= derived2;

        cout << " --> 10 --> array(0) = " << array(0) << endl;
        cout << " --> 11 --> array(1) = " << array(1) << endl;
        cout << " --> 12 --> array(2) = " << array(2) << endl;

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

	return 0;
}

//======//
// FINI //
//======//

