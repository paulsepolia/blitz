
//==============================//
// simple.cpp  			//    
// Some simple array operations //
//==============================//

//===============//
// code --> 0008 //
//===============//

// keywords: tensor, Array

#include <iostream>
#include <blitz/array.h>

using std::cout;
using std::cin;
using std::endl;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	BZ_USING_NAMESPACE(blitz::tensor)

	Array<double,1> x(100);
	firstIndex i;
        x = blitz::tensor::i; // x = [ 0, 1, 2, ..., 99 ]

    	Array<double,1> z(x + 150);
    	Array<double,1> v(z + x * 2);


	cout << " x = " << endl;
	cout << x << endl;

	cout << " z = " << endl;
	cout << z << endl;

	cout << " v = " << endl;
    	cout << v << endl;

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;
}

//======//
// FINI //
//======//

