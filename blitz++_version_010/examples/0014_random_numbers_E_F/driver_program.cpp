
//================================//
// random.cpp                     //
// Blitz++ random numbers example //
//================================//

//===============//
// code --> 0014 //
//===============//

// keywords: uniform, ranlib, random numbers

#include <random/uniform.h>
#include <blitz/numinquire.h>
#include <time.h>
#include <iostream>
#include <iomanip>

using namespace ranlib;
using namespace blitz;

// --> a template function

template<typename T>
void printRandoms()
{
 	Uniform<T> x;

  	//x.seed((unsigned int)time(0));

  	x.seed(5);

  	int N = 5;

  	for (int i = 0; i < N; ++i) 
    	{
		cout << setprecision(digits10(T())) << x.random() << endl;
	}

  	cout << endl;
}

// the main function

int main()
{
	// test get/set state interface
  
	Uniform<double> x;
  	Uniform<double>::T_state S = x.getState();

  	x.setState(S);

  	std::string str = x.getStateString();

  	x.setState(str);

  	cout << " --> some random float --> " << endl;

  	printRandoms<float>();

	cout << " --> some random doubles --> " << endl;

  	printRandoms<double>();

  	cout << " --> some random long doubles --> " << endl;
  
	printRandoms<long double>();

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

  	return 0;
}

//======//
// FINI //
//======//

