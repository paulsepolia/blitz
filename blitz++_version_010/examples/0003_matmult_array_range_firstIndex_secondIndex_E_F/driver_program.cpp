
//=============================================//
//  matmult.cpp     			       //
//  Blitz++ tensor notation example	       //
//  This example illustrates a matrix product  //
//  provided by Blitz++                        //
//=============================================//

//===============//
// code --> 0003 //
//===============//

// keywords: Range, Array, firstIndex, secondIndex, thirdIndex, fourthIndex 

#include <blitz/array.h>

// Kludge for KAI C++ 3.2c

#ifndef M_PI
 #define M_PI 3.14159265358979323846264338327950288
#endif

BZ_USING_NAMESPACE(blitz)

int main()
{
	const int N1 = 2000;
	const int N2 = 10000;

	cout << " --> 1 --> declare matrices --> A and B" << endl;

    	Range r(1, N1);
    	Array<double, 2> A(r,r);
	Array<double, 2> B(r,r);

    	// The first will be a Hilbert matrix:
    	//
    	// a   =   1
    	//  ij   -----
    	//       i+j-1
    	//
    	// Blitz++ provides a set of types { firstIndex, secondIndex, ... }
    	// which act as placeholders for indices.  These can be used directly
    	// in expressions.  For example, we can fill out the A matrix like this:

    	firstIndex i;    // Placeholder for the first index
    	secondIndex j;   // Placeholder for the second index
	thirdIndex k;

	cout << " --> 2 --> build matrix --> A" << endl;

    	A = 1.0 / (i+j-1);

    	// Now the A matrix has each element equal to a_ij = 1/(i+j-1)

	cout << " --> 3 --> build matrix --> B" << endl;

    	B = (i == (5-j));

    	//cout << "B = " << B << endl;

	cout << " --> 4 --> declare matrix --> C" << endl;

    	Array<double, 2> C(r,r);

	cout << " --> 5 --> build matrix --> C = Dot[A,B]" << endl;

	// the following expression computes the matrix product of A and B

    	C = sum(A(i,k) * B(k,j), k);

    	// Now let's fill out a two-dimensional array with a radially symmetric
    	// decaying sinusoid

	cout << " --> 6 --> declare martix --> F" << endl;

    	Array<double,2> F(N2,N2);
    	double midpoint = (N2-1)/2.0;
    	int cycles = 3;
    	double omega = 2.0 * double(M_PI) * cycles / double(N2);
    	double tau = - 10.0 / N2;

	cout << " --> 7 --> build matrix --> F" << endl;

    	F = cos(omega * sqrt(pow2(i-midpoint) + pow2(j-midpoint))) *
            exp(tau * sqrt(pow2(i-midpoint) + pow2(j-midpoint)));

	cout << " --> 8 --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> 9 --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

