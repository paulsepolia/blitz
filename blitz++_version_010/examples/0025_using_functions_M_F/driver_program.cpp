
//===========================================//
// define functions and apply them on arrays //
//===========================================//

//===============//
// code --> 0025 //
//===============//

// keywords: declare new functions

#define BZ_NEW_EXPRESSION_TEMPLATES

#include <blitz/array.h>

BZ_USING_NAMESPACE(blitz)
BZ_USING_NAMESPACE(blitz::tensor)

// function --> fA

double fA(double x)
{ 
	return 1.0 / (1 + x); 
}

// function --> fB

double fB(double x, double y)
{
    return x*y;
}

// declare the above function

BZ_DECLARE_FUNCTION(fA)
BZ_DECLARE_FUNCTION2(fB)

// the main function

int main()
{
	firstIndex i;
	secondIndex j;

    	Array<double,2> A(4,4), B(4,4), C(4,4);

    	A = 0,  1,  2,  3,
            4,  5,  6,  7,
            8,  9, 10, 11,
       	    12, 13, 14, 15;

    	C = 3;

    	cout << " --> A = " << A << endl
             << " --> C = " << C << endl;

    	B = fA(A);

    	cout << " --> B = fA(A) = " << endl;
    	cout << B << endl;

    	B = fB(A, C);

    	cout << " --> B = fB(A,C) = " << endl;

    	cout << B << endl;

    	B = fB(tensor::i, tensor::j);

    	cout << " --> B = fB(tensor::i, tensor::j) = " << B << endl;

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

	return 0;
}

//======//
// FINI //
//======//

