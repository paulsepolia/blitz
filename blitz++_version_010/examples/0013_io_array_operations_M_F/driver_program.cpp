
//=========================================//
// I/O operations using arrays and streams //
//=========================================//

//===============//
// code --> 0013 //
//===============//

// keywords: ofstream, ifstreami, tensor, array 

#include <blitz/array.h>
#include <iostream>
#include <cstdlib>

#ifdef BZ_HAVE_STD
	#include <fstream>
#else
	#include <fstream.h>
#endif

using std::cout;
using std::cin;
using std::endl;
using std::exit;

BZ_USING_NAMESPACE(blitz)
BZ_USING_NAMESPACE(blitz::tensor)

// global variable

const char* filename = "io.data";

// function

void write_arrays()
{
	// the indices

	firstIndex i;
	secondIndex j;
	thirdIndex k;

	// open filestream

    	ofstream ofs(filename);
    
	// test if opened ok

	if (ofs.bad())
    	{
        	cout << "Unable to write to file: " << filename << endl;
        	exit(1);
    	}

	// declare the array

    	Array<double,3> A(3,4,5);

	// build the array A

    	A = 111 + blitz::tensor::i + 10 * tensor::j + 100 * tensor::k;

	// output the array

    	ofs << A << endl;

	// declare array B

    	Array<float,2> B(3,4);

	// build array B

    	B = 11 + tensor::i + 10 * tensor::j;

	// output array B

    	ofs << B << endl;

	// declare array C

    	Array<float,1> C(4);

	// build array C

    	C = 1 + tensor::i;

	// write to file array C

    	ofs << C << endl;
}

// the main function

int main()
{
	// call the function

    	write_arrays();

	// open the input stream

    	ifstream ifs(filename);
    
	// test the stream

	if (ifs.bad())
    	{
        	cout << "Unable to open file: " << filename << endl;
        	exit(1);
    	}

	// declare the arrays

    	Array<float,3> A;
    	Array<float,2> B;
    	Array<float,1> C;

	// read for file

    	ifs >> A >> B >> C;

	// final statement

    	cout << "Arrays restored from file: " << endl << A << B << C << endl;

	// sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

