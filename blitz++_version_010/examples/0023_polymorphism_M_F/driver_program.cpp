
//============================//
// polymorphic arrays example //
//============================//

//===============//
// code --> 0023 //
//===============//

// keywords: 

#include <blitz/array.h>

using namespace blitz;

// class --> Material

class Material 
{
public:
    
	virtual double density() const = 0;
};

// class --> Air

class Air : public Material 
{
public:
    
	virtual double density() const 
	{ 
		return 0.291; 
	}
};

// class --> Water

class Water : public Material
{
public:
    
	virtual double density() const 
	{ 
		return 0.335; 
	}
};

// the main function

int main()
{
	Array<Water,1> A(4);
	Array<Air,1> B(4);
	Array<Material*,1> M(4);

	cout << A(1).density() << endl;
	cout << B(1).density() << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

