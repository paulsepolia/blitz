
//=============//
// convole.cpp //
//=============//

//===============//
// code --> 0021 //
//===============//

// keywords: convole, Array, Range

#include <blitz/array.h>
#include <blitz/array/convolve.h>

using namespace blitz;

// the main function

int main()
{
	Array<double,1> B(Range(-2,+2));
    	Array<double,1> C(Range(10,15));

    	B = 1, 0, 2, 5, 3;
    	C = 10, 2, 4, 1, 7, 2;

    	Array<double,1> A = convolve(B,C);

    	cout << " --> A has domain " << A.lbound(0) << "..." << A.ubound(0) << endl;
      	cout << " --> A = " << A << endl;

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

