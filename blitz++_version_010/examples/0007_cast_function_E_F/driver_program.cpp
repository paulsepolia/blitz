
//===============================//
// cast.cpp        	         //
// Blitz++ cast function example //
//===============================//

//===============//
// code --> 0007 //
//===============//

// keywords: cast function

#include <iostream>
#include <ctime>
#include <iomanip>
#include <blitz/array.h>

using std::endl;
using std::cout;
using std::cin;
using std::setprecision;
using std::fixed;
using std::showpos;
using std::showpoint;

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	// local parameters and variables

	const int K_MAX = 100000;
	const int N = 20000;
	firstIndex i;
	secondIndex j;
	clock_t t1;
	clock_t t2;

	// adjust the output format

	cout << fixed;
	cout << showpos;
	cout << showpoint;
	cout << setprecision(6);

	// main for loop

	for (int k = 0; k != K_MAX; k++)
	{
		cout << "--------------------------------------------------->> " << k << endl;

		cout << " -->  1 --> declare arrays" << endl;

    		Array<int,2> A(N,N);
		Array<double,2> B(N,N);

		cout << " -->  1 --> A = 10" << endl;

		t1 = clock();

    		A = 10;

		t2 = clock();

                cout << " --> time used = " 
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " -->  2 --> B = A" << endl;

		t1 = clock();

		B = A;

		t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

	       	cout << " -->  3 --> B = cast<double>(A)" << endl;

                t1 = clock();

                B = cast<double>(A);

                t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " -->  4 --> B = cast<int>(cast<double>(A))" << endl;

                t1 = clock();

                B = cast<int>(cast<double>(A));

                t2 = clock();

                cout << " --> time used = "
                     << (t2-t1)/static_cast<double>(CLOCKS_PER_SEC) << endl;

		cout << " -->  5 --> last" << endl;
	}

	cout << " --> end" << endl;

	// sentineling

	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

