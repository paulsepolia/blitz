
//=======================================//
// example of using Array<T,N>::iterator //
//=======================================//

//===============//
// code --> 0015 //
//===============//

// keywords: tensor, iterator, array, position

#include <blitz/array.h>

BZ_USING_NAMESPACE(blitz)

// the main function

int main()
{
	// create a 4x4 array and fill it with some numbers

    	Array<int,2> A(4,4);
    
	A = tensor::i * 10 + tensor::j; 
    
	cout << "A = " << A << endl;

    	// use an iterator to list the array elements

    	Array<int,2>::iterator iter = A.begin();
	Array<int,2>::iterator end = A.end();

    	while (iter != end)
   	{
        	cout << iter.position() << '\t' << (*iter) << endl;
        	++iter;
    	}

	// sentineling

	cout << " --> end" << endl;
	
	int sentinel;
	cin >> sentinel;

	cout << " --> exit" << endl;

    	return 0;
}

//======//
// FINI //
//======//

